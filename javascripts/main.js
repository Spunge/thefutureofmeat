(function($) {
	$(function() {
		/**
		 * Scroll to a content block, takes in a click event from link
		 */
		function scroll_to_block(e) {
			e.preventDefault();

			$('.menu').removeClass('js-active');

			// Get target block from anchor href
			var $id = $(e.currentTarget).attr('href');
			var $target = $('#' + $id);

			$('html, body').animate({
				scrollTop: $target.offset().top
			}, 500, 'swing');
		}

		// Scroll to correct block on menu click
		$('.menu a').on('click', scroll_to_block);
			
		// Make menu toggle
		$('.menu-toggle').on('click', function(e) {
			e.preventDefault();

			$('.menu').toggleClass('js-active');
		});
	});
})(jQuery);
